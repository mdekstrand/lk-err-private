This package contains the scripts and sources to drive Michael Ekstrand and
John Riedl's RecSys 2012 paper “When Recommenders Fail: Identifying and
Predicting Recommender Failure for Evidence-Based Algorithm Selection and
Combination”, updated for LensKit 2.1 as used in Michael's Ph.D dissertation.

Dependencies:

- Java 7

- R

- Various R packages.  If you use R from Anaconda, they can be installed from `environment.yml`

To run using Anaconda:

1.  Download the ML-10M data set from http://grouplens.org/datasets/movielens
    and unpack it into a directory called `data/ml-10m` (the `ratings.dat` file
    should reside directly in `data/ml-10m`).

2.  Create the Conda environment:

        conda env create -f environment.yml

    And then activate it:

        source activate rs-fail

3.  Run and analyze the experiment with `./gradlew run`.

This will produce a file `build/analysis.html` that contains the interesting
analysis outputs.  It will also save the analysis results to
`build/analysis.Rdata`, where they can be pulled into to knitr/Sweave scripts.

This version does not contain the LaTeX sources.  For that, check out the
`recsys-2012` branch.

You can also view the results in an IPython/Jupyter notebook by running

    jupyter notebook

and opening the `fail-paper.ipynb` notebook.
