package org.grouplens.lenskit.tags;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.grouplens.lenskit.cursors.Cursor;
import org.grouplens.lenskit.util.DelimitedTextCursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * @author Michael Ekstrand
 */
public class ImportTags {
    private static Logger logger = LoggerFactory.getLogger(ImportTags.class);

    public static void main(String[] args) throws IOException {
        File movieFile = new File(args[0]);
        File tagsFile = new File(args[1]);
        File indexFile = new File(args[2]);

        MovieDB movies = loadMovies(movieFile);
        loadTags(movies, tagsFile);

        Analyzer analyzer = new EnglishAnalyzer(Version.LUCENE_35);
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_35, analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        config.setRAMBufferSizeMB(512);

        Directory dir = FSDirectory.open(indexFile);
        IndexWriter index = new IndexWriter(dir, config);
        try {
            int n = indexMovies(index, movies);
            logger.info("Indexed {} movies", n);
            index.forceMerge(10);
        } finally {
            index.close();
        }
    }

    static DelimitedTextCursor openFile(File file) throws FileNotFoundException {
        FileInputStream input = new FileInputStream(file);
        Reader reader;
        try {
            reader = new InputStreamReader(input, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported", e);
        }
        BufferedReader buf = new BufferedReader(reader);
        return new DelimitedTextCursor(buf, "::");
    }

    static MovieDB loadMovies(File file) throws IOException {
        logger.info("reading movies from {}", file);
        MovieDB db = new MovieDB();
        Cursor<String[]> cursor = openFile(file);
        try {
            for (String[] row: cursor) {
                long id = Long.parseLong(row[0]);
                String title = row[1];
                Movie m = new Movie(id, title);
                String[] genres = row[2].split(",");
                for (String g: genres) {
                    m.addGenre(g);
                }
                db.addMovie(m);
            }
        } finally {
            cursor.close();
        }
        return db;
    }

    static void loadTags(MovieDB db, File file) throws IOException {
        logger.info("reading tags from {}", file);
        DelimitedTextCursor cursor = openFile(file);
        try {
            for (String[] row: cursor) {
                // user, item, tag, timestamp
                long movie = Long.parseLong(row[1]);
                String tag = row[2];
                Movie m = db.getMovie(movie);
                m.addTag(tag);
            }
        } finally {
            cursor.close();
        }
    }

    static int indexMovies(IndexWriter index, MovieDB db) throws IOException {
        int n = 0;
        for (Movie movie: db) {
            List<String> tags = movie.getTags();
            List<String> genres = movie.getGenres();

            logger.debug("indexing {} ({} tags, {} genres)",
                         new Object[]{movie.getTitle(), tags.size(), genres.size()});

            Document doc = new Document();

            Field docid = new Field("movie", Long.toString(movie.getId()),
                                    Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.NO);
            doc.add(docid);
            
            doc.add(new Field("title", movie.getTitle(),
                              Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
            
            doc.add(new Field("genres", StringUtils.join(genres, "\n"),
                              Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));

            doc.add(new Field("tags", StringUtils.join(tags, "\n"),
                              Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));

            index.addDocument(doc);
            n += 1;
        }
        return n;
    }
}
