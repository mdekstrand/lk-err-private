package org.grouplens.lenskit.tags.data;

import org.grouplens.lenskit.core.Parameter;

import javax.inject.Qualifier;
import java.io.File;
import java.lang.annotation.*;

/**
 * The file containing the user-user index.
 * @author <a href="http://www.grouplens.org">GroupLens Research</a>
 */
@Qualifier
@Parameter(File.class)
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface IndexFile {
}
