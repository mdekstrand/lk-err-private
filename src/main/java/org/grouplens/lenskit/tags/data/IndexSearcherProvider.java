package org.grouplens.lenskit.tags.data;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

import javax.inject.Inject;
import javax.inject.Provider;
import java.io.File;
import java.io.IOException;

/**
 * Open the item-item index file.
 * @author <a href="http://www.grouplens.org">GroupLens Research</a>
 */
public class IndexSearcherProvider implements Provider<IndexSearcher> {
    private final File indexDir;

    @Inject
    public IndexSearcherProvider(@IndexFile File dir) {
        indexDir = dir;
    }

    @Override
    public IndexSearcher get() {
        IndexReader idx;
        try (Directory dir = FSDirectory.open(indexDir)) {
            idx = IndexReader.open(new RAMDirectory(dir));
        } catch (IOException ex) {
            throw new RuntimeException("Cannot open index", ex);
        }
        return new IndexSearcher(idx);
    }
}
