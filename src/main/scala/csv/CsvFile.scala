package csv

import com.csvreader.CsvReader
import java.io._
import java.util.zip.GZIPInputStream
import grizzled.slf4j.Logging

/**
 * @author Michael Ekstrand
 */

class CsvFile(reader: CsvReader, hasHeaders: Boolean) extends Iterator[(Map[Symbol,String],Seq[String])] with Closeable with Logging {
  val headers =
    if (hasHeaders && reader.readHeaders())
      reader.getHeaders.map(Symbol(_))
    else
      Array[Symbol]()
  debug("headers: %s".format(headers.mkString(" ")))

  var current: Option[Array[String]] = None

  override def hasNext = {
    if (current.isDefined) {
      true
    } else if (reader.readRecord()) {
      current = Some(reader.getValues)
      true
    } else {
      reader.close()
      false
    }
  }

  override def next() = {
    if (!hasNext) throw new NoSuchElementException("past the end")
    val vals = current.get
    val map = headers.zip(vals).toMap
    val rest = vals.toSeq.drop(headers.length)
    current = None
    (map, rest)
  }

  def close() {
    reader.close()
  }
}

object CsvFile {
  def apply(input: Reader, hasHeaders: Boolean): CsvFile = new CsvFile(new CsvReader(input), hasHeaders)
  def apply(file: File, compressed: Boolean, hasHeaders: Boolean = true): CsvFile = {
    if (compressed) {
      val str = new FileInputStream(file)
      val decomp = new GZIPInputStream(str)
      val rdr = new InputStreamReader(decomp)
      apply(rdr, hasHeaders)
    } else {
      apply(new FileReader(file), hasHeaders)
    }
  }
  def apply(str: String, compressed: Boolean): CsvFile = apply(new File(str), compressed)
}
