package db

import org.scalaquery.ql._
import extended.{ExtendedTable => Table}
import TypeMapper._

object DataSet extends Enumeration {
  val random = Value(0, "random")
  val time = Value(1, "time")
  def apply(n: String) = n match {
    case "random" | "ML10M-Random" => random
    case "time" | "10M-Time" => time
  }
}

object Ratings extends Table[(Long,Long,Double,Long)]("ratings") {
  val user = column[Long]("user", O NotNull)
  val item = column[Long]("item", O NotNull)
  val rating = column[Double]("rating", O NotNull)
  val timestamp = column[Long]("timestamp", O NotNull)
  override val * = user ~ item ~ rating ~ timestamp
}

class PartitionTable(name: String) extends Table[(Int,Int,Long,Long)](name) {
  val dataset = column[Int]("dataset", O NotNull)
  val partition = column[Int]("partition", O NotNull)
  val user = column[Long]("user", O NotNull)
  val item = column[Long]("item", O NotNull)
  override val * = dataset ~ partition ~ user ~ item
}

object TestRatings extends PartitionTable("test_ratings")

object TrainRatings extends PartitionTable("train_ratings")

object Predictions extends Table[(Int,Int,String,Int,Int,Double)]("predictions") {
  val dataset = column[Int]("dataset", O NotNull)
  val partition = column[Int]("partition", O NotNull)
  val algorithm = column[String]("algorithm", O NotNull)
  val user = column[Int]("user", O NotNull)
  val item = column[Int]("item", O NotNull)
  val prediction = column[Double]("prediction", O NotNull)
  override val * = dataset ~ partition ~ algorithm ~ user ~ item ~ prediction
}
