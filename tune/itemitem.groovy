import org.grouplens.lenskit.knn.item.ItemItemModel
import org.grouplens.lenskit.knn.item.ItemItemRatingPredictor
import org.grouplens.lenskit.knn.params.ModelSize
import org.grouplens.lenskit.knn.params.NeighborhoodSize
import org.grouplens.lenskit.norm.BaselineSubtractingNormalizer
import org.grouplens.lenskit.norm.VectorNormalizer
import org.grouplens.lenskit.params.NormalizerBaseline
import org.grouplens.lenskit.params.UserVectorNormalizer

import org.grouplens.lenskit.tags.data.IndexedItemItemModel
import org.grouplens.lenskit.baseline.BaselinePredictor
import org.grouplens.lenskit.baseline.ItemUserMeanPredictor

/* compare item-item neighborhood sizes */
trainTest {
    output "itemitem.csv"

    metric CoveragePredictMetric
    metric MAEPredictMetric
    metric RMSEPredictMetric
    metric NDCGPredictMetric

    dataset crossfold("1M") {
        source {
            file "ml-1m/ratings.dat"
            delimiter "::"
            domain {
                minimum 1
                maximum 5
                precision 0.5
            }
        }
        holdout 0.2
        partitions 5
    }

    for (n in [10,20,30,40,50,75,100]) {
        algorithm("ItemItem") {
            attributes["nnbrs"] = n
            setComponent(RatingPredictor, ItemItemRatingPredictor)
            setComponent(BaselinePredictor, ItemUserMeanPredictor)
            setComponent(UserVectorNormalizer, VectorNormalizer,
                         BaselineSubtractingNormalizer)
            setComponent(NormalizerBaseline, BaselinePredictor,
                         ItemUserMeanPredictor)
            set(ModelSize, 500)
            set(NeighborhoodSize, n)
            set(MeanSmoothing, 25)
        }
    }
}
