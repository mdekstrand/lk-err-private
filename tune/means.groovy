import org.grouplens.lenskit.knn.item.ItemItemModel
import org.grouplens.lenskit.knn.item.ItemItemRatingPredictor
import org.grouplens.lenskit.knn.params.ModelSize
import org.grouplens.lenskit.knn.params.NeighborhoodSize
import org.grouplens.lenskit.norm.BaselineSubtractingNormalizer
import org.grouplens.lenskit.norm.VectorNormalizer
import org.grouplens.lenskit.params.NormalizerBaseline
import org.grouplens.lenskit.params.UserVectorNormalizer

import org.grouplens.lenskit.tags.data.IndexedItemItemModel
import org.grouplens.lenskit.baseline.BaselinePredictor
import org.grouplens.lenskit.baseline.ItemUserMeanPredictor
import org.grouplens.lenskit.baseline.GlobalMeanPredictor
import org.grouplens.lenskit.baseline.UserMeanPredictor
import org.grouplens.lenskit.baseline.ItemMeanPredictor
import org.grouplens.lenskit.baseline.BaselineRatingPredictor
import org.grouplens.lenskit.RatingPredictor
import org.grouplens.lenskit.eval.metrics.predict.NDCGPredictMetric

/* analyze means for damping */
trainTest {
    output "means.csv"

    metric CoveragePredictMetric
    metric MAEPredictMetric
    metric RMSEPredictMetric
    metric NDCGPredictMetric

    dataset crossfold("10M") {
        source {
            file "ml-10m/ratings.dat"
            delimiter "::"
            domain {
                minimum 1
                maximum 5
                precision 0.5
            }
        }
        holdout 0.2
        partitions 5
        wrapper {
            new IndexedDAO.Factory(it, new File("ml-10m.idx"))
        }
    }

    def baselines = [
            GlobalMeanPredictor,
            UserMeanPredictor,
            ItemMeanPredictor,
            ItemUserMeanPredictor
    ]

    for (norm in baselines) {
        for (d in [0, 10, 15, 25, 30, 50, 100]) {
            algorithm(norm.simpleName.replace(/Predictor$/, "")) {
                attributes["damping"] = d
                setComponent(RatingPredictor, BaselineRatingPredictor)
                setComponent(BaselinePredictor, ItemUserMeanPredictor)
                set(MeanSmoothing, d)
            }
        }
    }
}
